import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class MoreArrays {
	// count up some characters
	public static int[] counts(URL url) throws IOException {
		// Initialize everything
		int[] array = new int[256];
		// connect to URL
		java.net.URLConnection connection = url.openConnection();
		// create a reader object
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		// now lets read from this thing
		String inputLine;
		// read from the reader
		// read in a line, and as long as there is a line...
		while ((inputLine = in.readLine()) != null) {
			
			/*
			// go through each character of the string
			for (char c : inputLine.toCharArray()) {
				// array[c] = array[c] + 1;
				// USE INT[]ARRAY = ... FOR THE LAB
				array[c]++;
			}
			// print it out
			System.out.println(inputLine);
		*/
		
		}
		
		// clean up
		// close the reader
		in.close();
		// this is our happy place
		return null;

	}

	public static void main(String[] args) throws IOException {
		int[] array = counts(new URL("http://www.gutenberg.org/cache/epub/11/pg11.txt"));
		System.out.println(array['.']);
		System.out.println(array[',']);
		System.out.println(array[' ']);
	}
}
