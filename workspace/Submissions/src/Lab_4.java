import java.util.Scanner;
import java.text.*;

public class Lab_4 { // Part 1
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Interest Calculator");
		System.out.println("Enter the current price of an item ($):");
		double price = in.nextDouble();
		System.out.println("Enter the interest rate (%):");
		double intrate = in.nextDouble();
		System.out.println("Enter the number of years:");
		double years = in.nextDouble();
		double interest = intrate * .01;
		double newprice = price * Math.pow(1 + (interest / 1), years);
		DecimalFormat decFor = new DecimalFormat("0.00");
		if (price <= 0) {
			System.out.println("Invalid price");
		} else if (intrate <= 0) {
			System.out.println("Invalid interest rate");
		} else if (years <= 0) {
			System.out.println("Invalid number of years");
		} else
			System.out.println("$" + decFor.format(newprice));

	}
}
