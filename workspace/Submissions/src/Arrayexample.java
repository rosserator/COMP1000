
public class Arrayexample {
	public static void main(String[] args) {
		// declare hello as a string
		String hello;
		// initialize: give it a value
		// quotes for strings
		hello = "Hal iz zee best";
		// declare and initialize in one line
		char[] array = hello.toUpperCase().toCharArray();
		// declare my code
		int code;
		// go through each letter in the array
		for (char letter : array) {
			code = (int) letter;
			// ignore characters outside of uppercase
			// || = 'or' (non-inclusive i guess)
			if (code < 'A' || code > 'Z') {
			} else {
				// 0 is the new 65, getting rid of the ascii value
				// code = code - 65;
				// this is shorthand for the above line
				code -= 65;
				// do the cipher
				code = (code + 1) % 26;
				// convert back to ascii
				// code = code + 65;
				code += 65;
			}
			System.out.print((char)code);
			
			// print out the character a corresponding unicode value
			// System.out.format("%c => %d\n", letter, (int)letter);
		}
		// this doesn't work, but it would produce the unicode value of that character if it did
		//System.out.format("%c => %d\n", '', (int)'');
		
		// apostrophes for single characters
		char something = 'h';
		
		//System.out.println(hello.charAt(0));
		/*
		 * error types:
		 * Syntax: the compiler sees something wrong
		 * semantic: the code misbehaves
		 * schematic: the code is messy
		 */
	}
}
