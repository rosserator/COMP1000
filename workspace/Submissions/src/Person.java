
// a class is a way of allowing us to define a structure (packaging several pieces of info)

import java.util.Date;

//It's conventional to name things a ceratin way:
//TypeNamesLookLikeThis
//varibaleNamesLookLikeThis
//ENUM_OPTIONS_ARE_LIKE_THIS
//MAGIC_NUMBERS_ARE_LIKE_THIS_TOO
//constantsLookLikethis
// class means cookie-cutter
// the object is the cookie
// the memory is the cookie dough
// we're defining a type (i.e. a class)
public class Person {
	String name;
	// If you make something final, you can't reassign it
	final Date birthday;

	// enum(eration) defines a new type
	// a type is a set of possibilities
	enum Gender {
		// the options for gender
		MALE, FEMALE, NOT_SPECIFIED;

		public String toString() {
			if (this == Person.Gender.MALE) {
				return "male";
			} else if (this == Person.Gender.FEMALE) {
				return "female";
			} else if (this == Person.Gender.NOT_SPECIFIED) {
				return "unknown";
			}
			return null;

		}
	};

	// this defies a variable of type Gender
	Gender gender;

	// a constructor is a method that initializes the object
	Person(String name, Date birthday, Person.Gender sex) {
		this.name = name;
		this.birthday = birthday;
		gender = sex;
	}

	public String toString() {
		return name + ", a " + gender + " was born on " + birthday;

	}

	public static void main(String[] args) {
		// here we're creating objects of type Person
		// here's the cookie
		// new is how we allocate memory to an object
		Person me = new Person("Joey", new Date(81, 3, 7), Person.Gender.MALE);

		// this line...
		Person you = new Person("You", new Date(96, 1, 2), Person.Gender.MALE);
		// ... is the same as these lines...
		/*
		 * you.birthday = new Date(); you.gender = Person.Gender.NOT_SPECIFIED;
		 * you.name = "You";
		 */
		System.out.println(me);
		System.out.println(you);

	}
}
