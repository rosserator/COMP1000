import java.util.Scanner;
import java.util.*;

public class UnitConverter {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);

		System.out.println("Unit Converter");
		System.out.println("Valid unit are: K, C, F, kg, g, oz, lbs, m, km, mi, sec, min, hr, day, yr");
		System.out.println("Enter a measurement");
		double measurement = in.nextDouble();
		System.out.println("Enter a unit");
		String unit;
		unit = in.next();
		System.out.println("Enter a new unit");
		String newunit;
		newunit = in.next();

		if (unit.equals("C") || unit.equals("K") || unit.equals("F")) {

			if (unit.equals("F")) {
				measurement = (measurement - 32) / (9 / 5); // convert F to C
				if (newunit.equals("K")) {
					measurement = (measurement - 273.15); // convert C to K
					System.out.println(measurement);
				} else
					System.out.println(measurement);

			} else if (unit.equals("K")) {
				measurement = (measurement + 273.15); // K to C
				if (newunit.equals("F")) {
					measurement = (measurement + 32) / (5 / 9);
					System.out.println(measurement);
				} else
					System.out.println(measurement);
			} else if (unit.equals("C")) {
				if (newunit.equals("K")) {
					measurement = measurement - 273.15;
					System.out.println(measurement);
				} else if (newunit.equals("F")) {
					measurement = (measurement + 32) * (9 / 5);
					System.out.println(measurement);
				}
			}

		} else if (unit.equals("kg") || unit.equals("lbs") || unit.equals("g") || unit.equals("oz")) {
			if (unit.equals("kg")) {
				measurement = measurement / 1000;
				if (newunit.equals("g")) {
					System.out.println(measurement);
				} else if (newunit.equals("lbs")) {
					measurement = measurement * 453.592;
					System.out.println(measurement);
				} else if (newunit.equals("oz")) {
					measurement = measurement * 28.3495;
					System.out.println(measurement);
				}
			} else if (unit.equals("lbs")) {
				measurement = measurement / 453.592;
				if (newunit.equals("g")) {
					System.out.println(measurement);
				} else if (newunit.equals("kg")) {
					measurement = measurement * 1000;
					System.out.println(measurement);
				} else if (newunit.equals("oz")) {
					measurement = measurement / 28.3495;
					System.out.println(measurement);
				}
			} else if (unit.equals("oz")) {
				measurement = measurement * 28.3495;
				if (newunit.equals("g")) {
					System.out.println(measurement);
				} else if (newunit.equals("kg")) {
					measurement = measurement * 1000;
					System.out.println(measurement);
				} else if (newunit.equals("lbs")) {
					measurement = measurement * .0625;
					System.out.println(measurement);
				}

			}

		} else if (unit.equals("m") || unit.equals("km") || unit.equals("mi")) {
			if (unit.equals("m")) {
				if (newunit.equals("km")) {
					measurement = measurement * 1000;
					System.out.println((measurement));
				} else if (newunit.equals("mi")) {
					measurement = measurement * 0.000621371;
					System.out.println(measurement);
				}
			} else if (unit.equals("km")) {
				measurement = measurement / 1000;
				if (newunit.equals("m")) {
					System.out.println(measurement);
				} else if (newunit.equals("mi")) {
					measurement = measurement * 0.000621371;
					System.out.println(measurement);
				}
			} else if (unit.equals("mi")) {
				measurement = measurement / 0.000621371;
				if (newunit.equals("m")) {
					System.out.println(measurement);
				} else if (newunit.equals("km")) {
					measurement = measurement * 1000;
					System.out.println(measurement);
				}
			}

		} else if (unit.equals("sec") || unit.equals("min") || unit.equals("hr") || unit.equals("day")
				|| unit.equals("yr")) {
			if (unit.equals("sec")) {
				if (newunit.equals("min")) {
					measurement = measurement / 60;
					System.out.println((measurement));
				} else if (newunit.equals("hr")) {
					measurement = measurement / 60 / 60;
					System.out.println(measurement);
				} else if (newunit.equals("day")) {
					measurement = measurement * 60 / 60 / 24;
					System.out.println(measurement);
				} else if (newunit.equals("yr")) {
					measurement = measurement * 60 / 60 / 24 / 365;
					System.out.println(measurement);
				}
			} else if (unit.equals("min")) {
				measurement = measurement * 60;
				if (newunit.equals("sec")) {
					System.out.println(measurement);
				} else if (newunit.equals("hr")) {
					measurement = measurement / 60 / 60;
					System.out.println(measurement);
				} else if (newunit.equals("day")) {
					measurement = measurement / 60 / 60 / 24;
					System.out.println(measurement);
				} else if (newunit.equals("yr")) {
					measurement = measurement / 60 / 60 / 24;
					System.out.println(measurement);
				}
			} else if (unit.equals("hr")) {
				measurement = measurement * 60 * 60;
				if (newunit.equals("sec")) {
					System.out.println(measurement);
				} else if (newunit.equals("min")) {
					measurement = measurement / 60;
					System.out.println(measurement);
				} else if (newunit.equals("day")) {
					measurement = measurement / 60 / 60 / 24;
					System.out.println(measurement);
				} else if (newunit.equals("yr")) {
					measurement = measurement / 60 / 60 / 24 / 365;
					System.out.println(measurement);
				}
			} else if (unit.equals("day")) {
				measurement = measurement * 24 * 60 * 60;
				if (newunit.equals("sec")) {
					System.out.println(measurement);
				} else if (newunit.equals("min")) {
					measurement = measurement / 60;
					System.out.println(measurement);
				} else if (newunit.equals("hr")) {
					measurement = measurement / 60 / 60;
					System.out.println(measurement);
				} else if (newunit.equals("yr")) {
					measurement = measurement / 60 / 60 / 24 / 365;
					System.out.println(measurement);
				}
			} else if (unit.equals("yr")) {
				measurement = measurement * 24 * 60 * 60 * 365;
				if (newunit.equals("sec")) {
					System.out.println(measurement);
				} else if (newunit.equals("min")) {
					measurement = measurement / 60;
					System.out.println(measurement);
				} else if (newunit.equals("hr")) {
					measurement = measurement / 60 / 60;
					System.out.println(measurement);
				} else if (newunit.equals("day")) {
					measurement = measurement / 60 / 60 / 24;
					System.out.println(measurement);
				}
			} else System.out.println("Invalid");
		
		/*
		 * //convert from canonical unit measurement = measurement *
		 * conversionFactor(unit); // convert to canonical unit measurement =
		 * measurement * toUnit(desiredUnit); system.out.println(measurement);
		 * 
		 * 
		 * 
		 * 
		 * private static double conversionFactor(String unit) { if
		 * (unit.equals("km")) { return 1000; } else if (unit.equals("in")) {
		 * return .0254; } else if (unit.equals("cm")) { return .01; } return 0;
		 * }
		 * 
		 * /* if (measurement == 1.0) { System.out.println(
		 * "one is the loneliest number"); } if (unit.equals("km")) {
		 * System.out.println("Metric?!?!"); } else { System.out.println("hmm");
		 * }
		 */

	}
	}
}